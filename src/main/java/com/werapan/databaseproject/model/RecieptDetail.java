/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class RecieptDetail {

    private int recieptDetailId;
    private int productId;
    private String productName;
    private float productPrice;
    private int qty;
    private float totalPrice;
    private int recieptI;

    public RecieptDetail(int recieptDetailId, int productId, String productName, float productPrice, int qty, float totalPrice, int recieptI) {
        this.recieptDetailId = recieptDetailId;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptI = recieptI;
    }

    public RecieptDetail(int productId, String productName, float productPrice, int qty, float totalPrice, int recieptI) {
        this.recieptDetailId = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptI = recieptI;
    }

    public RecieptDetail() {
        this.recieptDetailId = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptI = 0;
    }

    public int getRecieptDetailId() {
        return recieptDetailId;
    }

    public void setRecieptDetailId(int recieptDetailId) {
        this.recieptDetailId = recieptDetailId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty * productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptI() {
        return recieptI;
    }

    public void setRecieptI(int recieptI) {
        this.recieptI = recieptI;
    }

    @Override
    public String toString() {
        return "RecieptDetailModel{" + "recieptDetailId=" + recieptDetailId + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptI=" + recieptI + '}';
    }

    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setRecieptDetailId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProductId(rs.getInt("product_id"));
            recieptDetail.setProductName(rs.getString("product_name"));
            recieptDetail.setProductPrice(rs.getFloat("product_price"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptI(rs.getInt("reciept_id"));
        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }

}
