/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.model.Customer;
import java.util.List;

/**
 *
 * @author werapan
 */
public class CustomerService {

    public Customer getByTel(String tel) {
        CustomerDao cusDao = new CustomerDao();
        Customer cus = cusDao.getByTel(tel);
        return cus;

    }

    public List<Customer> getCustomers() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getAll(" customer_id asc");
    }

    public Customer addNew(Customer editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.delete(editedCustomer);
    }
}
