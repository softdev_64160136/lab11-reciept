/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author werapan
 */
public class TestCusService {

    public static void main(String[] args) {
        CustomerService cus = new CustomerService();
        for (Customer customer : cus.getCustomers()) {
            System.out.println(customer);
        }
        System.out.println(cus.getByTel("0625204648"));

        Customer delCus = cus.getByTel("0949803060");
        delCus.setTel("0949801234");
        cus.update(delCus);
        System.out.println("After Update");
        for (Customer customer : cus.getCustomers()) {
            System.out.println(customer);
        }
        cus.delete(delCus);
        for (Customer customer : cus.getCustomers()) {
            System.out.println(customer);
        }
    }
}
